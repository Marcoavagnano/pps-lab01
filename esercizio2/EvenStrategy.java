package lab01.tdd;

public class EvenStrategy implements SelectStrategy{
    private final int value;
    public EvenStrategy(final int value){ this.value = value; }

    @Override
    public boolean apply(int element) {
        return element % value == 0;
    }
}