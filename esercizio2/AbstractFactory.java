package lab01.tdd;

public interface AbstractFactory{
    SelectStrategy create(String strategyType, int value);
}
