import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {
    private CircularList list;
    private  AbstractFactory abstractFactory;

    //TODO: test implementation

    @BeforeEach
    public void init(){
        this.abstractFactory= FactoryProvider.getFactory();
        this.list =new SimpleCircularList();
    }
    @Test
    public void testEmpty(){
        assertTrue(list.isEmpty());
    }
    @Test
    public void testNext(){
        list.add(0);
        list.add(1);
        assertEquals(Optional.of(0),list.next());
        assertEquals(Optional.of(1),list.next());
    }

    @Test
    public void testPrevious(){
        list.add(0);
        list.add(1);
        list.next();
        assertEquals(Optional.of(1),list.previous());
        assertEquals(Optional.of(0),list.previous());
    }
    @Test
    public void testReset(){
        list.add(0);
        list.next();
        list.add(1);
        list.next();
        list.reset();
        assertEquals(Optional.of(0),list.next());
    }
    @Test
    public void testEvenStrategy(){
        SelectStrategy evenStrategy = abstractFactory.create("EVEN",2);
        list.add(3);
        list.add(3);
        list.add(5);
        list.add(4);
        assertEquals(Optional.of(4),list.next(evenStrategy));
    }
    @Test
    public void testMultipleOfStrategy(){
        SelectStrategy multipleOfStrategy = abstractFactory.create("MUL",5);
        list.add(3);
        list.add(3);
        list.add(5);
        list.add(10);
        assertEquals(Optional.of(5),list.next(multipleOfStrategy));
    }
    @Test
    public void testEqualsStrategy(){
        SelectStrategy equalsStrategy = abstractFactory.create("EQUALS",10);
        list.add(3);
        list.add(3);
        list.add(5);
        list.add(10);
        assertEquals(Optional.of(10),list.next(equalsStrategy));
    }
}
