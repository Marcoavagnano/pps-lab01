package lab01.tdd;

public class StrategyFactory implements AbstractFactory{

    @Override
    public SelectStrategy create(String strategyType, int value) {
        switch (strategyType){
            case "EVEN":
                return new EvenStrategy(value);
            case "MUL":
                return new MultipleOfStrategy(value);
            case "EQUALS":
                return new EqualsStrategy(value);
        }
        return null;
    }
}
